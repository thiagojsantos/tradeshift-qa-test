class ProductPage < SitePrism::Page

  require_relative 'sections/navigation_bar'

  set_url "/gp/product/{id}"

  element :product_price, '#priceblock_ourprice'
  element :product_quantity, '#quantity'
  element :add_to_basket_button, '#add-to-cart-button'

  section :navigation_bar, NavigationBar, '#navbar'

  def add_to_basket(quantity = 1)
    product_quantity.select(quantity)
    store_product_info
    add_to_basket_button.click
  end

  def store_product_info
    collected_info = {:id => current_path[/product\/(.*)/, 1], :price =>price_format(product_price.text), :quantity => product_quantity.find('option[selected]').text}
    $products_info.push(collected_info)
  end

  def price_format(price)
    aux = price[/\$(.*)/, 1]
    if aux[-2,2] == "00"
      return aux[0..-4]
    else
      return aux
    end
  end
end