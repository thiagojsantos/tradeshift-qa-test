class BasketPage < SitePrism::Page

  set_url '/gp/cart/view.html'

  elements :basket_itens_list, '#activeCartViewForm > div.sc-list-body'

  section :basket_itens_section, '#activeCartViewForm > div.sc-list-body' do
    elements :products_images, 'div.sc-item-product-image'
    elements :products_titles, 'a.a-link-normal.sc-product-link'
    elements :action_links, 'div.a-row.sc-action-links'
    elements :price_text, 'div.a-column.a-span2.a-text-left'
    elements :quantity_dropdown, 'span.a-dropdown-prompt'
  end


  def get_current_basket_values
    product_info_in_basket = Array.new
    $products_info.each do |array_iten|
      BasketPage.new.basket_itens_list.each { |item|
        current_basket = item.find("div[data-asin=#{array_iten[:id]}]")
        product_info_in_basket.push({:id => current_basket['data-asin'], :price => current_basket['data-price'], :quantity => current_basket['data-quantity']})
      }
    end
    return product_info_in_basket
  end
end
