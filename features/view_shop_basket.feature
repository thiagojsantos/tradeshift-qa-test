Feature: View Shop Basket
  As a shopper on amazon.com
  I wish to view which product(s) are in my shopping basket

  Scenario: Addition of two different items and check for them in the basket
    Given I visit DualShock 4 Wireless Controller for PlayStation 4 - Jet Black product page
    When I add 1 product(s) to basket
    Given I visit Xbox One Wireless Controller product page
    When I add 1 product(s) to basket
    And I visit my basket
    Then the product(s) image, price, quantity, title link and delete/save for later options must be displayed
    And there should be the product(s) I have just added with the correct price and quantity