require 'capybara'
require 'capybara/cucumber'
require 'site_prism'
require 'yaml'
require 'rspec'
require 'selenium-webdriver'


PRODUCT_DATA = (YAML.load_file('./features/fixture/product_data.yml'))
$products_info = Array.new

Capybara.configure do |config|
  config.default_driver = :selenium
  config.app_host = 'https://www.amazon.com'
end

Capybara.register_driver :selenium do |app|
  client = Selenium::WebDriver::Remote::Http::Default.new
  client.timeout = 120
  Capybara::Selenium::Driver.new(app, browser: :chrome, http_client: client)
end

World(Capybara::DSL)
World(Capybara::RSpecMatchers)