When(/^I add (\d+) product\(s\) to basket$/) do |quantity|
  ProductPage.new.add_to_basket(quantity)
end

Then(/^there should be the product\(s\) I have just added with the correct price and quantity$/) do
  expect(BasketPage.new.get_current_basket_values).to eq($products_info), "Product(s) (information) added DO NOT match the current ones on Basket"
end


Then(/^the product\(s\) image, price, quantity, title link and delete\/save for later options must be displayed$/) do
  basket_page = BasketPage.new

  #wait for all list content to be displayed
  basket_page.wait_until_basket_itens_section_visible(10)


  expect(basket_page.basket_itens_section).to be_visible
  expect(basket_page.basket_itens_section).to have_products_images
  expect(basket_page.basket_itens_section).to have_products_titles

  #"delete"" and "save for later" options validation
  expect(basket_page.basket_itens_section.has_action_links?).to eq(true)
  expect(basket_page.basket_itens_section).to have_action_links count: 2

  #"quantity" dropdown and price validation
  expect(basket_page.basket_itens_section.has_price_text?).to eq(true)
  expect(basket_page.basket_itens_section.has_quantity_dropdown?).to eq(true)
end

Given(/^I visit (.*) product page$/) do |product_name|
  ProductPage.new.load(id: PRODUCT_DATA['products'][product_name]['id'])
end

And(/^I visit my basket$/) do
  ProductPage.new.navigation_bar.basket_link.click
end