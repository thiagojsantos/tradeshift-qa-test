# Tradeshift QA Test


### About the project

Tradeshift QA Test is part of Tradeshift Assingment and has been developed to be an initial effort in order to help Developers to go on with the testing automation. It has been built totally based on PageObject Pattern and has SitePrism Framework as a complement to achieve a good level maintainability by the use of not only Pages but also Sections
  
As for Test Case description, Cucumber Framework provides a high level os reuse of Test Steps and is totally based on user stories specifications. It's believed that putting both SitePrism and Cucumber together turns out to make the whole project easier to maintain in both test automation architecture and test readability
 
### Scenarios Description Document

The Scenarios Description Document is in the root location of the project:

* tradeshift_assignment.pdf

### Project Dependencies

* ruby 2.3.1
* gem 'cucumber'
* gem 'capybara'
* gem 'site_prism', '~> 2.9'
* gem 'selenium-webdriver'
* gem 'rspec'
* gem 'chromedriver-helper'

### Project Structure

               .
               +-- features
                  +-- fixture
                  |   +-- product_data.yml
                  +-- _pages
                  |   +-- sections
                  |      |+--navigation_bar.rb
                  |   +-- basket_page.rb
                  |   +-- product_page.rb
                  +-- step_definitions
                  |   +-- shopping_navigation_steps.rb
                  +-- support
                  |   +-- env.rb
                  +-- view_shop_basket.feature
                  +-- .gitignore
                  +-- tradeshift_assignment.pdf
                  +-- Gemfile
                  +-- Gemfile.lock
                  +-- README.mad
    
### Installation Instructions

* Use RVM to handle rubies and gemsets

 * See installation instructions here (https://rvm.io/rvm/install)
 
* Install and select ruby version
 
              $ rvm install ruby-2.3.1
              $ rvm use ruby-2.3.1

 
* Install bundler
 
             $ gem install bundler
 
* Project Installation
 
             $ git clone https://thiagojsantos@bitbucket.org/thiagojsantos/tradeshift-qa-test.git
             $ bundle


### Running Tests

            $ cucumber features/ 
            